import com.github.javafaker.Faker;

public class UserFactory {
    public static User generateDummyUser() {
        Faker faker = new Faker();
        UserBuilder userBuilder = new UserBuilder()
                .setEmail(faker.internet().emailAddress())
                .setId(faker.random().nextInt(0,99999))
                .setUsername(faker.letterify("??????_")+faker.name().username())
                .setFirstName(faker.name().firstName())
                .setLastName(faker.name().lastName())
                .setUserStatus(1)
                .setPassword(faker.internet().password())
                .setPhone(faker.phoneNumber().cellPhone());
        return userBuilder.createUser();
    }
}
