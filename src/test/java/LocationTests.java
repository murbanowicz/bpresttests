

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;


public class LocationTests {
    //TODO Add cucumber support
    //TODO Add spring dependency injection support
    //TODO Add more tests

    final String baseUrl = "https://petstore3.swagger.io/api/v3"; //TODO move to config file
    final String usersPath = "/user";

    @Before
    public void setup() {
        RestAssured.baseURI = baseUrl;
    }

    @Test
    public void addUserTest() {
        User dummyUser = UserFactory.generateDummyUser();

        this.addUser(dummyUser);
    }

    @Test
    public void addAndGetUser() {
        User dummyUser = UserFactory.generateDummyUser();

        User addedUser = this.addUser(dummyUser);

        Response getResponse = RestAssured.given().get(usersPath + "/" + dummyUser.getUsername());
        assertEquals(getResponse.statusCode(), 200);
    }

    @Test
    public void getNotExistingUser() {
        Faker faker = new Faker();
        Response getResponse = RestAssured.given().get(usersPath + "/" + faker.letterify("?????????????????????????????????"));
        assertEquals(getResponse.statusCode(), 404);
    }

    //TODO Test is failing add an issue to bugtracker, and provide link to the ticket here
    @Ignore
    @Test
    public void addSameUser() {
        User dummyUser = UserFactory.generateDummyUser();

        this.addUser(dummyUser);

        RequestSpecification httpRequest2 = RestAssured.given().
                contentType("application/json").
                body(dummyUser);

        Response response2 = httpRequest2.post(usersPath);

        assertNotEquals("It should not be possible to add same user.", response2.statusCode(), 200);
    }

    @Test
    public void removeUser() {
        User dummyUser = UserFactory.generateDummyUser();
        User addedUser = this.addUser(dummyUser);
        Response deleteResponse = RestAssured.given().delete(usersPath + "/" + dummyUser.getUsername());
        assertEquals(deleteResponse.statusCode(), 200);
    }

    @Test
    public void tryToGetRemovedUser() {
        User dummyUser = UserFactory.generateDummyUser();
        this.addUser(dummyUser);
        Response deleteResponse = RestAssured.given().delete(usersPath + "/" + dummyUser.getUsername());
        assertEquals(deleteResponse.statusCode(), 200);
    }

    private User addUser(User user) {
        RequestSpecification httpRequest = RestAssured.given().
                contentType("application/json").
                body(user);

        Response response = httpRequest.post(usersPath);
        User returnedUser = response.as(User.class);

        assertEquals(response.statusCode(), 200);
        assertTrue(user.equals(returnedUser));

        return returnedUser;
    }
}
